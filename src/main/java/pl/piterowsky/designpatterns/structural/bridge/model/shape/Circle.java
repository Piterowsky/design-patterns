package pl.piterowsky.designpatterns.structural.bridge.model.shape;

import pl.piterowsky.designpatterns.structural.bridge.model.renderer.Renderer;

public class Circle extends Shape {

    public float radius;

    public Circle(Renderer renderer) {
        super(renderer);
    }

    public Circle(Renderer renderer, float radius) {
        super(renderer);
        this.radius = radius;
    }

    @Override
    public void draw() {
        renderer.renderCircle(radius);
    }

    @Override
    public void resize(float factor) {
        radius *= factor;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

}
