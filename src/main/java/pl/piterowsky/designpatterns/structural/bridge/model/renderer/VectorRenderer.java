package pl.piterowsky.designpatterns.structural.bridge.model.renderer;

public class VectorRenderer implements Renderer {

    @Override
    public void renderCircle(float radius) {
        System.out.println("Rendering a vector circle of radius: " + radius);
    }

}
