package pl.piterowsky.designpatterns.structural.bridge.model.renderer;

public class RasterRenderer implements Renderer {

    @Override
    public void renderCircle(float radius) {
        System.out.println("Rendering a raster circle of radius: " + radius);
    }

}
