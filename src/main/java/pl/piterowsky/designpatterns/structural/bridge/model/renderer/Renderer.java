package pl.piterowsky.designpatterns.structural.bridge.model.renderer;

public interface Renderer {

    void renderCircle(float radius);

}
