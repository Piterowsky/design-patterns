package pl.piterowsky.designpatterns.structural.bridge.model.shape;

import com.google.inject.AbstractModule;
import pl.piterowsky.designpatterns.structural.bridge.model.renderer.Renderer;
import pl.piterowsky.designpatterns.structural.bridge.model.renderer.VectorRenderer;

public class ShapeModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Renderer.class).to(VectorRenderer.class);
    }

}
