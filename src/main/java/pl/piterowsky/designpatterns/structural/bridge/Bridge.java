package pl.piterowsky.designpatterns.structural.bridge;

import com.google.inject.Guice;
import pl.piterowsky.designpatterns.structural.bridge.model.renderer.RasterRenderer;
import pl.piterowsky.designpatterns.structural.bridge.model.renderer.VectorRenderer;
import pl.piterowsky.designpatterns.structural.bridge.model.shape.Circle;
import pl.piterowsky.designpatterns.structural.bridge.model.shape.ShapeModule;

/**
 * <pre>
 * <b>Decouple</b> abstraction from implementation
 * Stronger form of <b>encapsulation</b>
 * </pre>
 */
public class Bridge {

    public static void main(String[] args) {
        var injector = Guice.createInjector(new ShapeModule());
        var instance = injector.getInstance(Circle.class);
        instance.setRadius(5);
        instance.draw();

        var vectorRenderer = new VectorRenderer();
        var vectorCircle = new Circle(vectorRenderer, 5f);
        vectorCircle.draw();

        var rasterRenderer = new RasterRenderer();
        var rasterCircle = new Circle(rasterRenderer, 5f);
        rasterCircle.draw();
    }

}
