package pl.piterowsky.designpatterns.structural.bridge.model.shape;

import pl.piterowsky.designpatterns.structural.bridge.model.renderer.Renderer;

public abstract class Shape {

    protected final Renderer renderer;

    protected Shape(Renderer renderer) {
        this.renderer = renderer;
    }

    public abstract void draw();
    
    public abstract void resize(float factor);

}
