package pl.piterowsky.designpatterns.structural.composite.products.tv;

import pl.piterowsky.designpatterns.structural.composite.products.Product;

public class TV implements Product {

    @Override
    public double getPrice() {
        return 400;
    }

}
