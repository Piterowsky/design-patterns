package pl.piterowsky.designpatterns.structural.composite.products;

public interface Product {

    double getPrice();

}
