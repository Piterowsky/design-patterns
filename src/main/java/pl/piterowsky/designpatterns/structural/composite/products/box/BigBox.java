package pl.piterowsky.designpatterns.structural.composite.products.box;

public class BigBox extends AbstractBox {

    public BigBox() {
        super(50);
    }

}
