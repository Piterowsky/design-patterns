package pl.piterowsky.designpatterns.structural.composite.products.phone;

import pl.piterowsky.designpatterns.structural.composite.products.Product;

public class CellPhone implements Product {

    @Override
    public double getPrice() {
        return 200;
    }

}
