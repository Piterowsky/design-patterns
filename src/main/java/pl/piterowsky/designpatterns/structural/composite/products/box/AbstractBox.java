package pl.piterowsky.designpatterns.structural.composite.products.box;

import org.apache.commons.lang3.StringUtils;
import pl.piterowsky.designpatterns.structural.composite.products.Product;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBox implements Product {

    private List<Product> products = new ArrayList<>();

    private final double boxPrice;

    protected AbstractBox(double boxPrice) {
        this.boxPrice = boxPrice;
    }

    public void add(Product product) {
        products.add(product);
    }

    public void remove(Product product) {
        products.remove(product);
    }

    @Override
    public double getPrice() {
        return boxPrice + products.stream().map(Product::getPrice).reduce(0d, Double::sum);
    }

    public void printPricesOfIndividualProduct(int indent) {
        System.out.println(StringUtils.repeat(" ", indent) + "Box: " + this.boxPrice);

        products.forEach(product -> {
            if(product instanceof AbstractBox) {
                var box = (AbstractBox) product;
                box.printPricesOfIndividualProduct(indent + 1);
            } else {
                System.out.println(StringUtils.repeat(" ", indent + 1) + "Product: " + product.getPrice());
            }
        });
    }

}
