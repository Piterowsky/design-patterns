package pl.piterowsky.designpatterns.structural.composite;

import pl.piterowsky.designpatterns.structural.composite.products.box.BigBox;
import pl.piterowsky.designpatterns.structural.composite.products.box.SmallBox;
import pl.piterowsky.designpatterns.structural.composite.products.phone.CellPhone;
import pl.piterowsky.designpatterns.structural.composite.products.phone.PhoneBattery;
import pl.piterowsky.designpatterns.structural.composite.products.tv.TV;

/**
 * Composite pattern allows to combine objects into a tree hierarchy
 * and then we can work with this tree as if they were individual objects
 */
public class Composite {

    public static void main(String[] args) {
        var phoneBox = new SmallBox();
        phoneBox.add(new CellPhone());
        phoneBox.add(new PhoneBattery());

        var orderBox = new BigBox();
        orderBox.add(phoneBox);
        orderBox.add(new TV());

        orderBox.printPricesOfIndividualProduct(0);
        System.out.println("Price of entire orderBox: " + orderBox.getPrice());
    }

}
