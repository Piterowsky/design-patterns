package pl.piterowsky.designpatterns.structural.composite.products.box;

public class SmallBox extends AbstractBox{

    public SmallBox() {
        super(10);
    }

}
