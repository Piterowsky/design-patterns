package pl.piterowsky.designpatterns.structural.composite.products.phone;

import pl.piterowsky.designpatterns.structural.composite.products.Product;

public class PhoneBattery implements Product {

    @Override
    public double getPrice() {
        return 100;
    }

}
