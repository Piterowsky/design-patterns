package pl.piterowsky.designpatterns.structural.adapter;

import pl.piterowsky.designpatterns.structural.adapter.model.CartesianPoint;
import pl.piterowsky.designpatterns.structural.adapter.model.PolarPoint;

/**
 * Wzorzec polegający na ukryciu konwersji obiektu
 */
public class Adapter {

    public static void main(String[] args) {
        ExistingAPI.drawPoint(new CartesianPoint(1, 1));
        var adapter = new PolarToCartesianAdapter(new PolarPoint(1.4142, 45));
        var adapter1 = new PolarToCartesianAdapter(new PolarPoint(2, 45));
        ExistingAPI.drawPoint(adapter.toCartesianPoint());
        ExistingAPI.drawPoint(adapter.toCartesianPoint());
        ExistingAPI.drawPoint(adapter1.toCartesianPoint());
    }

}
