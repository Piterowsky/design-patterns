package pl.piterowsky.designpatterns.structural.adapter.model;

import lombok.Data;

@Data
public class CartesianPoint {

    private double x;
    private double y;

    public CartesianPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

}