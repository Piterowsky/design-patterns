package pl.piterowsky.designpatterns.structural.adapter.model;

import lombok.Data;

@Data
public class PolarPoint {

    private double r;
    private double radians;

    public PolarPoint(double r, double radians) {
        this.r = r;
        this.radians = Math.toRadians(radians);
    }

}
