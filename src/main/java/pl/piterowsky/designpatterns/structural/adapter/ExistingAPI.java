package pl.piterowsky.designpatterns.structural.adapter;

import pl.piterowsky.designpatterns.structural.adapter.model.CartesianPoint;

public class ExistingAPI {

    public static void drawPoint(CartesianPoint cartesianPoint) {
        System.out.printf("Drawing point [%f:%f]%n", cartesianPoint.getX(), cartesianPoint.getY());
    }

}
