package pl.piterowsky.designpatterns.structural.adapter;

import lombok.EqualsAndHashCode;
import pl.piterowsky.designpatterns.structural.adapter.model.CartesianPoint;
import pl.piterowsky.designpatterns.structural.adapter.model.PolarPoint;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode
public class PolarToCartesianAdapter {

    private static Map<Integer, CartesianPoint> cache = new HashMap<>();

    private final PolarPoint polarPoint;
    private final int hashcode;

    public PolarToCartesianAdapter(PolarPoint polarPoint) {
        this.polarPoint = polarPoint;
        this.hashcode = hashCode();
    }

    public CartesianPoint toCartesianPoint() {
        if(cache.containsKey(hashcode)) {
            System.out.println("Got from cache");
            return cache.get(hashcode);
        }
        var r = polarPoint.getR();
        var theta = polarPoint.getRadians();
        var decimalPlaces = 2;

        var x = round(r * Math.cos(theta), decimalPlaces);
        var y = round(r * Math.sin(theta), decimalPlaces);

        var cartesianPoint = new CartesianPoint(x, y);
        cache.put(hashcode, cartesianPoint);
        return cartesianPoint;
    }

    private double round(double value, int decimalPlaces) {
        if (decimalPlaces < 0) {
            throw new IllegalArgumentException("number of decimal places cannot be negative");
        }
        return BigDecimal.valueOf(value).setScale(decimalPlaces, RoundingMode.HALF_EVEN).doubleValue();
    }

}
