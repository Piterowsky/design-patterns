package pl.piterowsky.designpatterns.structural.flyweight;

import lombok.Getter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

// Shared state
@Getter
@ToString
public class TreeType {

    private static final Map<String, TreeType> types = new HashMap<>();

    private final String name;
    private final Size size;
    private final Color color;

    private TreeType(String name, Size size, Color color) {
        this.name = name;
        this.size = size;
        this.color = color;
    }

    public static TreeType getOrAdd(String name, Size size, Color color) {
        if (types.containsKey(name)) {
            return types.get(name);
        }
        var treeType = new TreeType(name, size, color);
        types.put(name, treeType);
        return treeType;
    }

    public enum Size {
        BIG_TREE, SMALL_TREE, MEDIUM_TREE
    }

    public enum Color {
        GREEN, WHITE, ORANGE
    }


}
