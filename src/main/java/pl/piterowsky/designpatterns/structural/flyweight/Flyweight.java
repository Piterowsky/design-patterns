package pl.piterowsky.designpatterns.structural.flyweight;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static pl.piterowsky.designpatterns.structural.flyweight.TreeType.Color;
import static pl.piterowsky.designpatterns.structural.flyweight.TreeType.Size;

/**
 * Helps to decrease of ram usage when dealing with many objects containing similar fields
 */
public class Flyweight {

    public static void main(String[] args) {
        var smallGreenTree = TreeType.getOrAdd("small-green-tree", Size.SMALL_TREE, Color.GREEN);
        var mediumOrangeTree = TreeType.getOrAdd("medium-orange-tree", Size.MEDIUM_TREE, Color.ORANGE);
        var random = new Random();

        var trees = IntStream.range(0, 10000).boxed().map(i -> {
            var treeType = random.nextDouble() > 0.5 ? smallGreenTree : mediumOrangeTree;
            var x = random.nextInt(1001);
            var y = random.nextInt(1001);
            return new Tree(x, y, treeType);
        }).collect(Collectors.toList());

        trees.stream().map(Tree::getTreeType).distinct().forEach(System.out::println);
    }

}
