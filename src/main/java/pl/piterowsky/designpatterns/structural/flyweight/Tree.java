package pl.piterowsky.designpatterns.structural.flyweight;

import lombok.AllArgsConstructor;
import lombok.Getter;

// Unique state
@Getter
@AllArgsConstructor
public class Tree {

    private final int x;
    private final int y;
    private final TreeType treeType;

}
