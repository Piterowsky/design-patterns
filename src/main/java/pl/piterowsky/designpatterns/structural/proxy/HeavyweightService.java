package pl.piterowsky.designpatterns.structural.proxy;

public interface HeavyweightService {

    default void perform() {
        System.out.println("Preforming heavy operation");
    }

}
