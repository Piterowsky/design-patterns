package pl.piterowsky.designpatterns.structural.proxy;

/**
 * Used when we want to delay initialization of some heavyweight service object
 * to keep lower use of system resources to the time when service is actually needed
 */
public class VirtualProxy {

    private HeavyweightService service;

    public void useService() {
        service = new HeavyweightServiceImpl();
        service.perform();
    }

    public static void main(String[] args) {
        var proxy = new VirtualProxy();
        proxy.useService();
    }
}


