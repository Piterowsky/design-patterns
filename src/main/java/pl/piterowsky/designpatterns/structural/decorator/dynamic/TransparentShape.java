package pl.piterowsky.designpatterns.structural.decorator.dynamic;

public class TransparentShape implements Shape{

    private final Shape shape;
    private final int transparency;

    public TransparentShape(Shape shape, int transparency) {
        this.shape = shape;
        this.transparency = transparency;
    }

    @Override
    public String info() {
        return shape.info() + ", with transparency " + transparency;
    }

}
