package pl.piterowsky.designpatterns.structural.decorator.generic;

public class Circle implements Shape {

    private final int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public String info() {
        return "This is a circle of radius " + radius;
    }

}
