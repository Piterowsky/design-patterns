package pl.piterowsky.designpatterns.structural.decorator.generic;

import java.util.function.Supplier;

public class ColoredShape<T extends Shape> implements Shape {

    private final Shape shape;
    private final String color;

    public ColoredShape(Supplier<? extends T> shapeSupplier, String color) {
        this.shape = shapeSupplier.get();
        this.color = color;
    }

    @Override
    public String info() {
        return shape.info() + ", has color " + color;
    }

}
