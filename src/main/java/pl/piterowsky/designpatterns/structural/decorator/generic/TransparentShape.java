package pl.piterowsky.designpatterns.structural.decorator.generic;

import java.util.function.Supplier;

public class TransparentShape<T extends Shape> implements Shape {

    private final Shape shape;
    private final int transparency;

    public TransparentShape(Supplier<? extends T> supplierShape, int transparency) {
        this.shape = supplierShape.get();
        this.transparency = transparency;
    }

    @Override
    public String info() {
        return shape.info() + ", with transparency " + transparency;
    }

}
