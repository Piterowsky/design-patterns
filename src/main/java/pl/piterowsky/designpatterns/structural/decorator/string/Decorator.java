package pl.piterowsky.designpatterns.structural.decorator.string;

public class Decorator {

    public static void main(String[] args) {
        var kajaczek = new StringDecorator("kajaczek");
        System.out.println(kajaczek.reverse());
    }

}
