package pl.piterowsky.designpatterns.structural.decorator.dynamic;

public class DynamicDecorator {

    public static void main(String[] args) {
        var circle = new TransparentShape(new ColoredShape(new Circle(5), "blue"), 50);
        var circle1 = new ColoredShape(new TransparentShape(new Circle(5), 50), "blue");
        System.out.println(circle.info());
        System.out.println(circle1.info());
    }

}
