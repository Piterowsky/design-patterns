package pl.piterowsky.designpatterns.structural.decorator.generic;

public class StaticDecorator {

    public static void main(String[] args) {
        var circle = new TransparentShape<>(() -> new ColoredShape<>(() -> new Circle(5), "blue"), 50);
        System.out.println(circle.info());
    }

}
