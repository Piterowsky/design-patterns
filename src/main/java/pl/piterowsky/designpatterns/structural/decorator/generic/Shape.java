package pl.piterowsky.designpatterns.structural.decorator.generic;

public interface Shape {

    String info();

}
