package pl.piterowsky.designpatterns.structural.decorator.dynamic;

public interface Shape {

    String info();

}
