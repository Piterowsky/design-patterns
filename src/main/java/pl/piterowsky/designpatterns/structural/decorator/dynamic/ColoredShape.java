package pl.piterowsky.designpatterns.structural.decorator.dynamic;

public class ColoredShape implements Shape {

    private final Shape shape;
    private final String color;

    public ColoredShape(Shape shape, String color) {
        this.shape = shape;
        this.color = color;
    }

    @Override
    public String info() {
        return shape.info() + ", has color " + color;
    }

}
