package pl.piterowsky.designpatterns.creational.prototype;

/**
 * Wzorzec jest używany kiedy tworzymy obiekty na podstawie już istniejących obiektów
 */
public class Prototype {

    public static void main(String[] args) {
        var person = new Person();
        person.setName("Person");
        person.setAddress(new Address("Street", "City"));

        //
        var copy = person.serializationDeepCopy();
        copy.setName("Serialization way copy");
        copy.getAddress().setStreet("Serialization way city");

        var nativeCopy = person.constructorDeepCopy();
        nativeCopy.setName("Native copy");
        nativeCopy.getAddress().setStreet("Native street copy");

        System.out.println(copy);
        System.out.println(nativeCopy);
        System.out.println(person);

    }

}
