package pl.piterowsky.designpatterns.creational.prototype;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class Address implements Serializable {

    private String street;
    private String city;

    public Address(String street, String city) {
        this.street = street;
        this.city = city;
    }

    public Address(Address address) {
        this.street = address.street;
        this.city = address.city;
    }

}
