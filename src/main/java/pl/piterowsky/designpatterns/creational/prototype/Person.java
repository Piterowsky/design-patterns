package pl.piterowsky.designpatterns.creational.prototype;

import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;

@Data
@ToString
public class Person implements Serializable {

    private String name;
    private Address address;

    public Person serializationDeepCopy() {
        return SerializationUtils.roundtrip(this);
    }

    public Person constructorDeepCopy() {
        var copy = new Person();
        copy.name = this.name;
        copy.address = new Address(this.address);
        return copy;
    }

}
