package pl.piterowsky.designpatterns.creational.multitone;

import lombok.ToString;

import java.util.EnumMap;
import java.util.Map;

@ToString
public class Multiton {

    private static Map<MultitonType, Multiton> instances = new EnumMap<>(MultitonType.class);

    private MultitonType type;

    private Multiton(MultitonType type) {
        this.type = type;
    }

    public static Multiton getInstance(MultitonType multitonType) {
        if(instances.containsKey(multitonType)) {
            return instances.get(multitonType);
        }
        var instance = new Multiton(multitonType);
        instances.put(multitonType, instance);
        return instance;
    }

    public static void main(String[] args) {
        var m0 = Multiton.getInstance(MultitonType.ZERO);
        var m1 = Multiton.getInstance(MultitonType.ZERO);
        var m2 = Multiton.getInstance(MultitonType.ONE);
        var m3 = Multiton.getInstance(MultitonType.TWO);

        System.out.println(m0 == m1);
        System.out.println(m0 != m2);
        System.out.println(m1 != m2);
        System.out.println(m0 != m3);
        System.out.println(m1 != m3);
    }

}
