package pl.piterowsky.designpatterns.creational.multitone;

public enum MultitonType {

    ZERO, ONE, TWO

}
