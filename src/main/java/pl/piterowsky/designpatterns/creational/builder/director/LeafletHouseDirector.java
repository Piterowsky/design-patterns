package pl.piterowsky.designpatterns.creational.builder.director;

import pl.piterowsky.designpatterns.creational.builder.builder.HouseBuilder;
import pl.piterowsky.designpatterns.creational.builder.model.Leaflet;

public class LeafletHouseDirector {

    private final HouseBuilder leafletHouseBuilder;

    public LeafletHouseDirector(HouseBuilder houseBuilder) {
        this.leafletHouseBuilder = houseBuilder;
    }

    public Leaflet make() {
        return (Leaflet) leafletHouseBuilder.addWindows().addFloor().build();
    }

}
