package pl.piterowsky.designpatterns.creational.builder.builder;

import lombok.ToString;
import pl.piterowsky.designpatterns.creational.builder.model.HouseBuilderProduct;
import pl.piterowsky.designpatterns.creational.builder.model.Leaflet;

@ToString
public class HouseLeafletBuilder implements HouseBuilder {

    private final Leaflet leaflet;

    public HouseLeafletBuilder() {
        this.leaflet = new Leaflet();
    }

    @Override
    public HouseBuilder addWalls() {
        leaflet.setWallsInfo("informations-about-walls");
        return this;
    }

    @Override
    public HouseBuilder addFloor() {
        leaflet.setFloorInfo("informations-about-floor");
        return this;
    }

    @Override
    public HouseBuilder addRoof() {
        leaflet.setRoofInfo("informations-about-roof");
        return this;
    }

    @Override
    public HouseBuilder addWindows() {
        leaflet.setWindowsInfo("informations-about-windows");
        return this;
    }

    @Override
    public HouseAddressBuilder withAddress() {
        return null;
    }

    @Override
    public HouseBuilderProduct build() {
        return leaflet;
    }


}
