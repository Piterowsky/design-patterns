package pl.piterowsky.designpatterns.creational.builder.builder;

import pl.piterowsky.designpatterns.creational.builder.model.HouseBuilderProduct;

public interface HouseBuilder {

    HouseBuilder addWalls();

    HouseBuilder addFloor();

    HouseBuilder addRoof();

    HouseBuilder addWindows();

    HouseAddressBuilder withAddress();

    HouseBuilderProduct build();

}
