package pl.piterowsky.designpatterns.creational.builder;

import pl.piterowsky.designpatterns.creational.builder.builder.ExpensiveHouseBuilder;
import pl.piterowsky.designpatterns.creational.builder.builder.HouseLeafletBuilder;
import pl.piterowsky.designpatterns.creational.builder.builder.StandardHouseBuilder;
import pl.piterowsky.designpatterns.creational.builder.director.HouseDirector;
import pl.piterowsky.designpatterns.creational.builder.director.LeafletHouseDirector;
import pl.piterowsky.designpatterns.creational.builder.model.Address;

import java.util.logging.Logger;

/**
 * Wzorzec pozwalający na utworzenie obiektu w kilku krokach.
 *
 * Możemy rozszerzać buildera {@link StandardHouseBuilder} i {@link ExpensiveHouseBuilder}.
 * Builder może zwracać różne typy {@link HouseLeafletBuilder}.
 */
public class BuilderPattern {

    private static final Logger LOGGER = Logger.getLogger(BuilderPattern.class.getName());

    public static void main(String[] args) {
        var cheapDirector = new HouseDirector(new StandardHouseBuilder<>());
        var expensiveDirector = new HouseDirector(new ExpensiveHouseBuilder());
        var leafletHouseDirector = new LeafletHouseDirector(new HouseLeafletBuilder());
        LOGGER.info(cheapDirector.make()::toString);
        LOGGER.info(cheapDirector.make(new Address("Szczecin", "00-000", "Matejki"))::toString);
        LOGGER.info(expensiveDirector.make()::toString);
        LOGGER.info(leafletHouseDirector.make()::toString);
    }

}





