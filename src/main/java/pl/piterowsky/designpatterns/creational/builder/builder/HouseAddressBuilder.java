package pl.piterowsky.designpatterns.creational.builder.builder;

import pl.piterowsky.designpatterns.creational.builder.model.Address;
import pl.piterowsky.designpatterns.creational.builder.model.House;

public class HouseAddressBuilder extends StandardHouseBuilder<HouseAddressBuilder> {

    private final Address address;

    public HouseAddressBuilder(House house) {
        this.house = house;
        this.address = new Address();
        house.setAddress(address);
    }

    public HouseAddressBuilder street(String street) {
        address.setStreet(street);
        return this;
    }

    public HouseAddressBuilder city(String city) {
        address.setCity(city);
        return this;
    }

    public HouseAddressBuilder postalCode(String postalCode) {
        address.setPostalCode(postalCode);
        return this;
    }

}
