package pl.piterowsky.designpatterns.creational.builder.director;

import pl.piterowsky.designpatterns.creational.builder.builder.ExpensiveHouseBuilder;
import pl.piterowsky.designpatterns.creational.builder.model.House;

public class ExpensiveHouseDirector {

    private final ExpensiveHouseBuilder houseBuilder;

    public ExpensiveHouseDirector(ExpensiveHouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    public House make() {
        return houseBuilder
                .addRoof()
                .addWalls()
                .addWindows()
                .addFloor()
                .addFacility("some-expensive-facility1")
                .addFacility("some-expensive-facility2")
                .addFacility("some-expensive-facility3")
                .build();
    }

}
