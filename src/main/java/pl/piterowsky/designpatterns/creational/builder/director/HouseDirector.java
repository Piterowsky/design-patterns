package pl.piterowsky.designpatterns.creational.builder.director;

import pl.piterowsky.designpatterns.creational.builder.builder.HouseBuilder;
import pl.piterowsky.designpatterns.creational.builder.model.Address;
import pl.piterowsky.designpatterns.creational.builder.model.House;

public class HouseDirector {

    private final HouseBuilder houseBuilder;

    public HouseDirector(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }

    public House make() {
        return (House) houseBuilder
                .addRoof()
                .addWalls()
                .addWindows()
                .addFloor()
                .build();
    }

    public House make(Address address) {
        return (House) houseBuilder
                .addRoof()
                .addWalls()
                .addWindows()
                .addFloor()
                .withAddress()
                .city(address.getCity())
                .postalCode(address.getPostalCode())
                .street(address.getStreet())
                .build();
    }

}
