package pl.piterowsky.designpatterns.creational.builder.builder;

import lombok.ToString;
import pl.piterowsky.designpatterns.creational.builder.model.House;
import pl.piterowsky.designpatterns.creational.builder.model.HouseBuilderProduct;

/**
 * Recursive generics allows us to return builder {@link T}
 * that may extends {@link StandardHouseBuilder} and preserve
 * "fluency" in builder
 */
@ToString
public class StandardHouseBuilder<T extends StandardHouseBuilder<T>> implements HouseBuilder {

    protected House house;

    public StandardHouseBuilder() {
        this.house = new House();
    }

    @Override
    public T addWalls() {
        house.setWalls("some-cheap-walls");
        return (T) this;
    }

    @Override
    public T addFloor() {
        house.setFloor("some-cheap-floor");
        return (T) this;
    }

    @Override
    public T addRoof() {
        house.setRoof("no-roof");
        return (T) this;
    }

    @Override
    public T addWindows() {
        house.setWindows("no-windows");
        return (T) this;
    }

    @Override
    public HouseAddressBuilder withAddress() {
        return new HouseAddressBuilder(house);
    }

    @Override
    public HouseBuilderProduct build() {
        return house;
    }


}