package pl.piterowsky.designpatterns.creational.builder.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Leaflet implements HouseBuilderProduct {

    private String wallsInfo;
    private String floorInfo;
    private String roofInfo;
    private String windowsInfo;

}