package pl.piterowsky.designpatterns.creational.builder.builder;

import lombok.ToString;
import pl.piterowsky.designpatterns.creational.builder.model.House;

import java.util.ArrayList;

@ToString
public class ExpensiveHouseBuilder extends StandardHouseBuilder<ExpensiveHouseBuilder> {

    private final House house;

    public ExpensiveHouseBuilder() {
        this.house = new House();
    }

    @Override
    public ExpensiveHouseBuilder addRoof() {
        house.setRoof("expensive-roof");
        return this;
    }

    @Override
    public ExpensiveHouseBuilder addWindows() {
        house.setWindows("expensive-windows");
        return this;
    }

    public ExpensiveHouseBuilder addFacility(String facility) {
        var facilities = house.getFacilities() != null ? house.getFacilities() : new ArrayList<String>();
        facilities.add(facility);
        house.setFacilities(facilities);
        return this;
    }

    public House build() {
        return house;
    }

}