package pl.piterowsky.designpatterns.creational.builder.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String city;
    private String postalCode;
    private String street;

}
