package pl.piterowsky.designpatterns.creational.builder.model;


import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class House implements HouseBuilderProduct {

    private String walls;
    private String floor;
    private String roof;
    private String windows;
    private List<String> facilities;
    private Address address;

}
