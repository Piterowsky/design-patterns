package pl.piterowsky.designpatterns.creational.abstractfactory.factory;

import pl.piterowsky.designpatterns.creational.abstractfactory.model.Drink;
import pl.piterowsky.designpatterns.creational.abstractfactory.model.IceTea;
import pl.piterowsky.designpatterns.creational.abstractfactory.model.Tea;

public class TeaFactory implements DrinkFactory {

    @Override
    public Drink prepareHotDrink() {
        System.out.println("Preparing tea");
        return new Tea();
    }

    @Override
    public Drink prepareColdDrink() {
        System.out.println("Preparing ice tea");
        return new IceTea();
    }

}
