package pl.piterowsky.designpatterns.creational.abstractfactory;

import pl.piterowsky.designpatterns.creational.abstractfactory.factory.CoffeeFactory;
import pl.piterowsky.designpatterns.creational.abstractfactory.factory.DrinkFactory;
import pl.piterowsky.designpatterns.creational.abstractfactory.factory.TeaFactory;
import pl.piterowsky.designpatterns.creational.abstractfactory.model.Drink;

public class HotDrinkMachine {

    private DrinkFactory factory;

    public HotDrinkMachine(HotDrinkType hotDrinkType) {
        switch (hotDrinkType) {
            case TEA:
                this.factory = new TeaFactory();
                break;
            case COFFEE:
                this.factory = new CoffeeFactory();
                break;
            default:
                throw new IllegalStateException("Specific factory for that drink type does not exist");
        }
    }

    public Drink makeHotDrink() {
        return factory.prepareHotDrink();
    }

    public Drink makeColdDrink() {
        return factory.prepareColdDrink();
    }

}
