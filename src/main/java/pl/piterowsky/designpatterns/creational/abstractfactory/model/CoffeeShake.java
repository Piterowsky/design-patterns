/*
 * Copyright 2021 Asseco Data Systems SA. All Rights Reserved.
 */
package pl.piterowsky.designpatterns.creational.abstractfactory.model;

/**
 * @author piotr.tatarski
 */
public class CoffeeShake implements Drink {
    @Override
    public void drink() {
        System.out.println("Drinking coffee shake");
    }
}
