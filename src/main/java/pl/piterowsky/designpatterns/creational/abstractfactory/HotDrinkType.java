package pl.piterowsky.designpatterns.creational.abstractfactory;

public enum HotDrinkType {
    COFFEE,
    TEA
}
