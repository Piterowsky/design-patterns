package pl.piterowsky.designpatterns.creational.abstractfactory.model;

public class IceTea implements Drink {

    @Override
    public void drink() {
        System.out.println("Drinking ice tea");
    }

}
