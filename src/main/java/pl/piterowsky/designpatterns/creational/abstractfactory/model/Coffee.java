package pl.piterowsky.designpatterns.creational.abstractfactory.model;

public class Coffee implements Drink {

    @Override
    public void drink() {
        System.out.println("Drinking coffee");
    }

}
