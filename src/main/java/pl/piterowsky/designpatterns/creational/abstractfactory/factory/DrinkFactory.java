package pl.piterowsky.designpatterns.creational.abstractfactory.factory;

import pl.piterowsky.designpatterns.creational.abstractfactory.model.Drink;

public interface DrinkFactory {

    Drink prepareHotDrink();

    Drink prepareColdDrink();

}
