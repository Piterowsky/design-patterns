package pl.piterowsky.designpatterns.creational.abstractfactory;

/**
 * Wzorzec pozwala na tworzennie powiązanych ze sobą
 * obiektów bez określania ich konkretnych implementacji
 */
public class AbstractFactory {

    public static void main(String[] args) {
        var coffeeMachine = new HotDrinkMachine(HotDrinkType.COFFEE);
        coffeeMachine.makeHotDrink().drink();
        coffeeMachine.makeColdDrink().drink();

        var teaMachine = new HotDrinkMachine(HotDrinkType.TEA);
        teaMachine.makeHotDrink().drink();
        teaMachine.makeColdDrink().drink();
    }

}
