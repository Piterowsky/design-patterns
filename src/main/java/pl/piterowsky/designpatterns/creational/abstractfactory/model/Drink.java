package pl.piterowsky.designpatterns.creational.abstractfactory.model;

public interface Drink {

    void drink();

}
