package pl.piterowsky.designpatterns.creational.abstractfactory.factory;

import pl.piterowsky.designpatterns.creational.abstractfactory.model.CoffeeShake;
import pl.piterowsky.designpatterns.creational.abstractfactory.model.Drink;
import pl.piterowsky.designpatterns.creational.abstractfactory.model.Coffee;

public class CoffeeFactory implements DrinkFactory {

    @Override
    public Drink prepareHotDrink() {
        System.out.println("Preparing coffee");
        return new Coffee();
    }

    @Override
    public Drink prepareColdDrink() {
        System.out.println("Preparing coffee shake");
        return new CoffeeShake();
    }

}
