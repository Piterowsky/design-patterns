package pl.piterowsky.designpatterns.creational.abstractfactory.model;

public class Tea implements Drink {

    @Override
    public void drink() {
        System.out.println("Drinking tea");
    }

}
