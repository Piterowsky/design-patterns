package pl.piterowsky.designpatterns.creational.singleton;

public class SingleThreadSingleton {

    private static SingleThreadSingleton instance;

    public static SingleThreadSingleton getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new SingleThreadSingleton();
        return instance;
    }

    public static void main(String[] args) {
        var i1 = SingleThreadSingleton.getInstance();
        var i2 = SingleThreadSingleton.getInstance();
        System.out.println(i1 == i2);
    }

}
