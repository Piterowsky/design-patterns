package pl.piterowsky.designpatterns.creational.singleton;

import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ThreadSafeSingleton {

    // Volatile ensure reading and writing to this variable
    // cannot be reordered by compiler while performing code optimizations
    private static volatile ThreadSafeSingleton instance;

    public static ThreadSafeSingleton getInstance() {
        // Reading volatile variables can be expensive
        var localRef = instance;

        // First check that let us skip synchronized block when instance is already initialized
        if (localRef != null) {
            return instance;
        }
        // Synchronized block to prevent thread race
        synchronized (ThreadSafeSingleton.class) {
            // Double check for case when one of the threads has already initialized instance
            if (localRef != null) {
                instance = new ThreadSafeSingleton();
            }
            return instance;
        }
    }

    public static void main(String[] args) {
        var threadPool = Executors.newFixedThreadPool(3);
        var futures = IntStream.range(0, 3)
                .boxed()
                .map(i -> threadPool.submit(ThreadSafeSingleton::getInstance))
                .collect(Collectors.toList());

        for (var future : futures) {
            System.out.println(futures.stream().allMatch(f -> {
                try {
                    return future.get() == f.get();
                } catch (Exception e) {
                    return false;
                }
            }));
        }
        threadPool.shutdownNow();
    }

}
