package pl.piterowsky.designpatterns.creational.singleton;

import org.apache.commons.lang3.SerializationUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Enum jest domyślnie serializowany (ale serializowana jest tylko nazwa
 * jeżeli enum ma jakieś pola, nie są one serializowane)
 */
public enum EnumBasedSingleton {
    INSTANCE;

    EnumBasedSingleton() {
        someNumber = 100;
    }

    private int someNumber;

    public void setSomeNumber(int someNumber) {
        this.someNumber = someNumber;
    }

    // Proof
    public static void main(String[] args) throws FileNotFoundException {
        var instance = EnumBasedSingleton.INSTANCE;
        var filename = "serializable.txt";

        instance.setSomeNumber(1); // Comment out at second run
        SerializationUtils.serialize(instance, new FileOutputStream(filename)); // Comment out at second run

        var deserializedEnum = (EnumBasedSingleton) SerializationUtils.deserialize(new FileInputStream(filename));

        System.out.println(instance.someNumber);
        System.out.println(deserializedEnum.someNumber);
    }
}
