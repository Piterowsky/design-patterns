package pl.piterowsky.designpatterns.creational.factorymethod;

/**
 * Wzorzec pozwalający na tworzenie obiektu tego samego typu, za pomocą konstruktora
 * o jednakowym nagłówku, ale na dwóch różnych zasadach. Plusem tego rozwiązania jest
 * jeszcze lepsza semantyka.
 */
public class FactoryMethod {

    public static void main(String[] args) {
        var cartesianPoint = Point.newCartesianPoint(1, 2);
        var polarPoint = Point.newPolarPoint(1, 2);
        System.out.println(cartesianPoint);
        System.out.println(polarPoint);
    }
}