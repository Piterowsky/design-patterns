package pl.piterowsky.designpatterns.behavioral.memento;

import lombok.ToString;

@ToString
public class State {

    private String privateStateValue;

    public void restore(Snapshot snapshot) {
        privateStateValue = snapshot.getPrivateStateValue();
    }

    public Snapshot makeSnapshot() {
        return new Snapshot(privateStateValue);
    }

    public void setPrivateStateValue(String privateStateValue) {
        this.privateStateValue = privateStateValue;
    }

    /**
     * Memento should be an immutable object, and only originator {@link State} class
     * should be able to use these memento objects
     */
    public static class Snapshot {

        private final String privateStateValue;

        public Snapshot(String privateStateValue) {
            this.privateStateValue = privateStateValue;
        }

        public String getPrivateStateValue() {
            return privateStateValue;
        }

    }

}
