package pl.piterowsky.designpatterns.behavioral.memento;

import java.util.ArrayList;
import java.util.List;

public class Caretaker {

    private State state;
    private List<State.Snapshot> snapshotList = new ArrayList<>();
    private int currentSnapshotIndex = 0;

    public Caretaker(State state) {
        this.state = state;
    }

    public void undo() {
        currentSnapshotIndex--;
        if(currentSnapshotIndex >= 0) {
            state.restore(snapshotList.get(currentSnapshotIndex));
        }
    }

    public void redo() {
        currentSnapshotIndex++;
        if(currentSnapshotIndex <= snapshotList.size() - 1) {
            state.restore(snapshotList.get(currentSnapshotIndex));
        }
    }

    public State.Snapshot save() {
        var snapshot = state.makeSnapshot();
        snapshotList.add(snapshot);
        currentSnapshotIndex = snapshotList.size() - 1;
        if(currentSnapshotIndex != snapshotList.size() - 1) {
            snapshotList = snapshotList.subList(0, snapshotList.size() - 1);
            currentSnapshotIndex = snapshotList.size() - 1;
        }
        return snapshot;
    }

    public State getState() {
        return state;
    }

    public List<State.Snapshot> getSnapshotList() {
        return snapshotList;
    }

}
