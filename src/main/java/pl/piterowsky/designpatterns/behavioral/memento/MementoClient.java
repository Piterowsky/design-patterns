package pl.piterowsky.designpatterns.behavioral.memento;


/**
 * Memento is a behavioral design pattern that lets you save and restore
 * the previous state of an object without revealing the details of its implementation.
 */
public class MementoClient {

    public static void main(String[] args) {
        var state = new State();
        var caretaker = new Caretaker(state);
        state.setPrivateStateValue("1");
        caretaker.save();
        state.setPrivateStateValue("2");
        caretaker.save();
        state.setPrivateStateValue("3");
        caretaker.save();

        System.out.println(state);
        caretaker.undo();
        System.out.println(state);
        caretaker.undo();
        System.out.println(state);
        caretaker.redo();
        System.out.println(state);
        caretaker.redo();
        System.out.println(state);
        caretaker.redo();
        System.out.println(state);
    }
}
