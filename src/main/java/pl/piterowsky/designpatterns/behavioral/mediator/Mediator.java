package pl.piterowsky.designpatterns.behavioral.mediator;

import pl.piterowsky.designpatterns.behavioral.mediator.component.Component;

public interface Mediator {

    void showDialog();

    void hideDialog();

    void registerComponents(Component component);

    View createView();

}
