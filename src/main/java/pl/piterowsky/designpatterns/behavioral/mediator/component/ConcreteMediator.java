package pl.piterowsky.designpatterns.behavioral.mediator.component;

import pl.piterowsky.designpatterns.behavioral.mediator.Mediator;
import pl.piterowsky.designpatterns.behavioral.mediator.View;


public class ConcreteMediator implements Mediator {

    private Button showButton;
    private Button hideButton;
    private Dialog dialog;

    @Override
    public void showDialog() {
        dialog.show();
        hideButton.setDisabled(true);
        showButton.setDisabled(false);
    }

    @Override
    public void hideDialog() {
        dialog.hide();
        hideButton.setDisabled(false);
        showButton.setDisabled(true);
    }

    @Override
    public void registerComponents(Component component) {
        switch (component.getType()) {
            case SPECIAL_DIALOG:
                dialog = (Dialog) component;
                break;
            case SHOW_BUTTON:
                showButton = (Button) component;
                break;
            case HIDE_BUTTON:
                hideButton = (Button) component;
                break;
            default:
                throw new RuntimeException("Incorrect component type for this mediator");
        }
    }

    @Override
    public View createView() {
        var components = new View();
        components.add(showButton);
        components.add(hideButton);
        components.add(dialog);
        return components;
    }

}
