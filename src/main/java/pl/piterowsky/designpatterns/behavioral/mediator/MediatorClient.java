package pl.piterowsky.designpatterns.behavioral.mediator;

import pl.piterowsky.designpatterns.behavioral.mediator.component.Button;
import pl.piterowsky.designpatterns.behavioral.mediator.component.ComponentType;
import pl.piterowsky.designpatterns.behavioral.mediator.component.ConcreteMediator;
import pl.piterowsky.designpatterns.behavioral.mediator.component.Dialog;

/**
 * Mediator is a design pattern for making less tightly coupling between components
 * Components speaks to themselves by class that implements mediator interface
 */
public class MediatorClient {

    public static void main(String[] args) {
        Mediator mediator = new ConcreteMediator();

        var hideButton = new Button("hide dialog", ComponentType.HIDE_BUTTON, mediator::hideDialog);
        var showButton = new Button("show dialog", ComponentType.SHOW_BUTTON, mediator::showDialog);
        var dialog = new Dialog(ComponentType.SPECIAL_DIALOG);

        showButton.setDisabled(true);

        mediator.registerComponents(showButton);
        mediator.registerComponents(hideButton);
        mediator.registerComponents(dialog);
        mediator.createView().printAllComponents();

        hideButton.getAction().run();

        mediator.createView().printAllComponents();


    }

}
