package pl.piterowsky.designpatterns.behavioral.mediator.component;

import pl.piterowsky.designpatterns.behavioral.mediator.Mediator;

public class Dialog implements Component {

    private final ComponentType componentType;

    private Mediator mediator;
    private boolean isHidden = false;

    public Dialog(ComponentType componentType) {
        this.componentType = componentType;
    }

    public void hide() {
        System.out.println("Hiding " + getType());
        isHidden = true;
    }

    public void show() {
        System.out.println("Showing " + getType());
        isHidden = false;
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    @Override
    public ComponentType getType() {
        return ComponentType.SPECIAL_DIALOG;
    }

    @Override
    public String toString() {
        return "Dialog{" +
                "componentType=" + componentType +
                ", isHidden=" + isHidden +
                '}';
    }

}
