package pl.piterowsky.designpatterns.behavioral.mediator.component;

import pl.piterowsky.designpatterns.behavioral.mediator.Mediator;

public interface Component {

    void setMediator(Mediator mediator);

    ComponentType getType();

}
