package pl.piterowsky.designpatterns.behavioral.mediator;

import pl.piterowsky.designpatterns.behavioral.mediator.component.Component;

import java.util.ArrayList;

public class View extends ArrayList<Component> {

    public void printAllComponents() {
        this.forEach(System.out::println);
    }



}
