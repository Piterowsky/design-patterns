package pl.piterowsky.designpatterns.behavioral.mediator.component;

import lombok.Data;
import pl.piterowsky.designpatterns.behavioral.mediator.Mediator;

@Data
public class Button implements Component {

    private final ComponentType componentType;
    private final Runnable action;
    private Mediator mediator;
    private String text;
    private boolean isDisabled = false;

    public Button(String text, ComponentType componentType, Runnable action) {
        this.text = text;
        this.componentType = componentType;
        this.action = action;
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    @Override
    public ComponentType getType() {
        return componentType;
    }

    @Override
    public String toString() {
        return "Button{" +
                "componentType=" + componentType +
                ", text='" + text + '\'' +
                ", isDisabled=" + isDisabled +
                '}';
    }

}
