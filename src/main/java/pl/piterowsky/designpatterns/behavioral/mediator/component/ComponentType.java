package pl.piterowsky.designpatterns.behavioral.mediator.component;

public enum ComponentType {

    SHOW_BUTTON, HIDE_BUTTON, SPECIAL_DIALOG

}
