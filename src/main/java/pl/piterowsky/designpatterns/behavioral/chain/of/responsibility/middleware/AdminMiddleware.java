package pl.piterowsky.designpatterns.behavioral.chain.of.responsibility.middleware;

public class AdminMiddleware extends Middleware {

    @Override
    public boolean check(String email, String password) {
        if (!email.equals("admin@test.com")) {
            System.out.println("Cannot access admin role");
            return false;
        }

        System.out.println("Admin role active");
        return checkNext(email, password);
    }

}
