package pl.piterowsky.designpatterns.behavioral.chain.of.responsibility.middleware;

import java.util.Set;

public class AuthMiddleware extends Middleware {

    private static final Set<String> VALID_EMAIL = Set.of("test@test.com", "admin@test.com");
    private static final String VALID_PASSWORD = "test";

    @Override
    public boolean check(String email, String password) {
        if (!VALID_EMAIL.contains(email) || !VALID_PASSWORD.equals(password)) {
            System.out.println("Incorrect email or password");
            return false;
        }

        System.out.println("Authenticated");
        return checkNext(email, password);
    }

}
