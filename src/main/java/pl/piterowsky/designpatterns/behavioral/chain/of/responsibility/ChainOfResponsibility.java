package pl.piterowsky.designpatterns.behavioral.chain.of.responsibility;

import pl.piterowsky.designpatterns.behavioral.chain.of.responsibility.middleware.AdminMiddleware;
import pl.piterowsky.designpatterns.behavioral.chain.of.responsibility.middleware.AuthMiddleware;
import pl.piterowsky.designpatterns.behavioral.chain.of.responsibility.middleware.LoggingMiddleware;

/**
 * Chain of responsibility provide a way for setting up chain of actions (even dynamically at a runtime)
 * Remember, only first element of the chain knows all of participants
 */
public class ChainOfResponsibility {

    public static void main(String[] args) {
        var firstMiddleware = new AuthMiddleware();
        var adminMiddleware = new AdminMiddleware();
        var loggingMiddleware = new LoggingMiddleware();

        firstMiddleware.linkWith(adminMiddleware).linkWith(loggingMiddleware);

        var result = firstMiddleware.check("admin@test.com", "test");

        System.out.println(result);
    }

}
