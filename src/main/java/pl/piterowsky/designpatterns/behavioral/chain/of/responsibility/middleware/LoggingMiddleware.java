package pl.piterowsky.designpatterns.behavioral.chain.of.responsibility.middleware;

public class LoggingMiddleware extends Middleware {

    @Override
    public boolean check(String email, String password) {
        System.out.println("Logging middleware");
        return checkNext(email, password);
    }

}
