package pl.piterowsky.designpatterns.behavioral.command.invoker;

import pl.piterowsky.designpatterns.behavioral.command.action.Command;

public class Shortcut extends Invoker {

    private String keys;

    public Shortcut(Command command, String keys) {
        super(command);
        this.keys = keys;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

}
