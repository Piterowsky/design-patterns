package pl.piterowsky.designpatterns.behavioral.command.action;

import pl.piterowsky.designpatterns.behavioral.command.Clipboard;

public class CopyCommand extends Command {

    private final String text;
    private final Clipboard clipboard;

    public CopyCommand(Clipboard clipboard, String text) {
        this.text = text;
        this.clipboard = clipboard;
    }

    @Override
    public void execute() {
        clipboard.setCopiedText(text);
        System.out.println("Copied text to clipboard: " + text);
    }

}
