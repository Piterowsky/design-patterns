package pl.piterowsky.designpatterns.behavioral.command.invoker;

import pl.piterowsky.designpatterns.behavioral.command.action.Command;

public class Invoker {

    protected Command command;

    protected Invoker(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void execute() {
        command.execute();
    }

}
