package pl.piterowsky.designpatterns.behavioral.command.invoker;

import pl.piterowsky.designpatterns.behavioral.command.action.Command;

public class Button extends Invoker {

    private String text;
    private double width;
    private double height;
    private String color;

    public Button(String text, Command command, double width, double height, String color) {
        super(command);
        this.text = text;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
