package pl.piterowsky.designpatterns.behavioral.command;

import pl.piterowsky.designpatterns.behavioral.command.action.CopyCommand;
import pl.piterowsky.designpatterns.behavioral.command.action.SaveCommand;
import pl.piterowsky.designpatterns.behavioral.command.invoker.Button;
import pl.piterowsky.designpatterns.behavioral.command.invoker.Shortcut;

/**
 * Command pattern lets to divide common and decouple action from its invokers
 */
public class Command {

    public static void main(String[] args) {
        var clipboard = new Clipboard();

        var saveCommand = new SaveCommand("content", "/some/path");
        var copyCommand = new CopyCommand(clipboard, "some copied text");

        var copyButton = new Button("Copy", copyCommand, 6.0, 2.0, "lightcoral");
        var saveButton = new Button("Save", saveCommand, 6.0, 2.0, "blue");
        var copyShortcut = new Shortcut(copyCommand, "ctrl+c");

        copyButton.execute();
        saveButton.execute();
        copyShortcut.execute();
    }

}
