package pl.piterowsky.designpatterns.behavioral.command;

public class Clipboard {

    public String copiedText;

    public String getCopiedText() {
        return copiedText;
    }

    public void setCopiedText(String copiedText) {
        this.copiedText = copiedText;
    }

}
