package pl.piterowsky.designpatterns.behavioral.command.action;

public class SaveCommand extends Command {

    private final String content;
    private final String path;

    public SaveCommand(String content, String path) {
        this.content = content;
        this.path = path;
    }

    @Override
    public void execute() {
        System.out.printf("Saving %s to %s\n", content, path);
    }

}
