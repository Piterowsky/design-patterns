package pl.piterowsky.designpatterns.behavioral.command.action;

public abstract class Command {

    public abstract void execute();

}
