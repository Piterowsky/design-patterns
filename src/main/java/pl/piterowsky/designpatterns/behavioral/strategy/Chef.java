package pl.piterowsky.designpatterns.behavioral.strategy;

import pl.piterowsky.designpatterns.behavioral.strategy.strategy.PizzaStrategy;

public class Chef {

    private final PizzaStrategy pizzaStrategy;

    public Chef(PizzaStrategy pizzaStrategy) {
        this.pizzaStrategy = pizzaStrategy;
    }

    public void makeGreatPizza() {
        System.out.println("Preparing ingredients");
        System.out.println(pizzaStrategy.getPizza());
        System.out.println("Give pizza to the customer");
    }

}
