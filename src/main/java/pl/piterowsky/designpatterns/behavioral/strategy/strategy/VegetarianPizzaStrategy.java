package pl.piterowsky.designpatterns.behavioral.strategy.strategy;

public class VegetarianPizzaStrategy implements PizzaStrategy{

    @Override
    public String getPizza() {
        return "Pizza with grass :D";
    }

}
