package pl.piterowsky.designpatterns.behavioral.strategy.strategy;

public class FourCheesesPizzaStrategy implements PizzaStrategy{

    @Override
    public String getPizza() {
        return "Four cheeses pizza";
    }

}
