package pl.piterowsky.designpatterns.behavioral.strategy.strategy;

public class SalamiPizzaStrategy implements PizzaStrategy{

    @Override
    public String getPizza() {
        return "Pizza with salami";
    }

}
