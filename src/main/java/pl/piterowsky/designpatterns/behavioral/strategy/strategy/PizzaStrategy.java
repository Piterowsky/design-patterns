package pl.piterowsky.designpatterns.behavioral.strategy.strategy;

public interface PizzaStrategy {

    String getPizza();

}
