/*
 * Copyright 2021 Asseco Data Systems SA. All Rights Reserved.
 */
package pl.piterowsky.designpatterns.behavioral.template.method;

/**
 * @author piotr.tatarski
 */
public class PoorSandwich extends AbstractSandwichAlgorithm {

    @Override
    protected void addSalad() {
        System.out.println("Adding ordinary salad...");
    }

    @Override
    protected void addTomato() {
        System.out.println("This sandwich doesn't has tomato");
    }

    @Override
    protected void addCheese() {
        System.out.println("Adding gouda cheese...");
    }

    @Override
    protected void addMeat() {
        System.out.println("Adding weenies...");
    }
}
