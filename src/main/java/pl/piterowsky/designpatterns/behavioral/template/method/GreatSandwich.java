/*
 * Copyright 2021 Asseco Data Systems SA. All Rights Reserved.
 */
package pl.piterowsky.designpatterns.behavioral.template.method;

/**
 * @author piotr.tatarski
 */
public class GreatSandwich extends AbstractSandwichAlgorithm {
    @Override
    public void addSalad() {
        System.out.println("Adding corn salad");
    }

    @Override
    protected void addTomato() {
        System.out.println("Adding raspberry tomato");
    }

    @Override
    protected void addCheese() {
        System.out.println("Adding mozzarella");
    }

    @Override
    public void addMeat() {
        System.out.println("Addling chicken...");
    }
}
