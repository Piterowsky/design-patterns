/*
 * Copyright 2021 Asseco Data Systems SA. All Rights Reserved.
 */
package pl.piterowsky.designpatterns.behavioral.template.method;

/**
 * @author piotr.tatarski
 */
public abstract class AbstractSandwichAlgorithm implements SandwichAlgorithm {

    @Override
    public void make() {
        cutRoll();
        addSalad();
        addCheese();
        addMeat();
        addTomato();
    }

    protected abstract void addSalad();

    protected abstract void addTomato();

    protected abstract void addCheese();

    protected abstract void addMeat();

    protected final void cutRoll() {
        System.out.println("Cutting roll...");
    }
}
