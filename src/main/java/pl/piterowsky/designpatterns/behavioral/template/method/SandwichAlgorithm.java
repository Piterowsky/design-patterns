/*
 * Copyright 2021 Asseco Data Systems SA. All Rights Reserved.
 */
package pl.piterowsky.designpatterns.behavioral.template.method;

/**
 * @author piotr.tatarski
 */
public interface SandwichAlgorithm {

    void make();

}
