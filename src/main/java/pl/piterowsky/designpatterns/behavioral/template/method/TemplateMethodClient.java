/*
 * Copyright 2021 Asseco Data Systems SA. All Rights Reserved.
 */
package pl.piterowsky.designpatterns.behavioral.template.method;

/**
 * @author piotr.tatarski
 */
public class TemplateMethodClient {
    public static void main(String[] args) {
        SandwichAlgorithm sandwichAlgorithm = Math.random() > 0.5 ? new GreatSandwich() : new PoorSandwich();
        sandwichAlgorithm.make();
    }
}
