package pl.piterowsky.designpatterns.behavioral.visitor.model;

import pl.piterowsky.designpatterns.behavioral.visitor.visitor.Visitor;

public class Swimmer implements Sportsman {

    @Override
    public void accept(Visitor visitor) {
        visitor.doForSwimmer(this);
    }

}
