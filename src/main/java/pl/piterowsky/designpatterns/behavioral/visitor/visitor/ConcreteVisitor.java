package pl.piterowsky.designpatterns.behavioral.visitor.visitor;

import pl.piterowsky.designpatterns.behavioral.visitor.model.Biker;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Runner;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Swimmer;

public class ConcreteVisitor implements Visitor {

    @Override
    public void doForRunners(Runner runner) {
        System.out.println("I'm runner");
    }

    @Override
    public void doForBiker(Biker biker) {
        System.out.println("I'm biker");
    }

    @Override
    public void doForSwimmer(Swimmer swimmer) {
        System.out.println("I'm swimmer");
    }

}
