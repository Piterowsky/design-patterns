package pl.piterowsky.designpatterns.behavioral.visitor.model;

import pl.piterowsky.designpatterns.behavioral.visitor.visitor.Visitor;

public interface Sportsman {

    void accept(Visitor visitor);

}
