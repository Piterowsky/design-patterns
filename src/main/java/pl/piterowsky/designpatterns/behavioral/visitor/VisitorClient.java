package pl.piterowsky.designpatterns.behavioral.visitor;

import pl.piterowsky.designpatterns.behavioral.visitor.model.Biker;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Runner;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Sportsman;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Swimmer;
import pl.piterowsky.designpatterns.behavioral.visitor.visitor.ConcreteVisitor;
import pl.piterowsky.designpatterns.behavioral.visitor.visitor.Visitor;

/**
 * Visitor pattern lets to separate algorithms of objects on which they operate
 */
public class VisitorClient {

    public static void main(String[] args) {
        Visitor visitor = new ConcreteVisitor();
        Sportsman runner = new Runner();
        Sportsman biker = new Biker();
        Sportsman swimmer = new Swimmer();

        runner.accept(visitor);
        biker.accept(visitor);
        swimmer.accept(visitor);

    }

}
