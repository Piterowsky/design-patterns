package pl.piterowsky.designpatterns.behavioral.visitor.model;

import pl.piterowsky.designpatterns.behavioral.visitor.visitor.Visitor;

public class Biker implements Sportsman {

    @Override
    public void accept(Visitor visitor) {
        visitor.doForBiker(this);
    }

}
