package pl.piterowsky.designpatterns.behavioral.visitor.model;

import pl.piterowsky.designpatterns.behavioral.visitor.visitor.Visitor;

public class Runner implements Sportsman {

    @Override
    public void accept(Visitor visitor) {
        visitor.doForRunners(this);
    }

}
