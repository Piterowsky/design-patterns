package pl.piterowsky.designpatterns.behavioral.visitor.visitor;

import pl.piterowsky.designpatterns.behavioral.visitor.model.Biker;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Runner;
import pl.piterowsky.designpatterns.behavioral.visitor.model.Swimmer;

public interface Visitor {

    void doForRunners(Runner runner);

    void doForBiker(Biker biker);

    void doForSwimmer(Swimmer swimmer);

}
