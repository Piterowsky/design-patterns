package pl.piterowsky.designpatterns.behavioral.observer;

import java.util.HashSet;
import java.util.Set;

public class Publisher {

    private final Set<Subscriber> subscribers = new HashSet<>();
    private final String name;

    public Publisher(String name) {
        this.name = name;
    }

    public void add(Subscriber subscriber) {
        System.out.println("Adding subscriber " + subscriber.getId() + " to publisher " + name);
        subscribers.add(subscriber);
    }

    public void remove(Subscriber subscriber) {
        System.out.println("Removing subscriber " + subscriber.getId() + " from publisher " + name);
        subscribers.remove(subscriber);
    }

    public void notifySubscribers() {
        System.out.println("Notifying subscribers of publisher " + name);
        subscribers.forEach(Subscriber::update);
    }
}
