package pl.piterowsky.designpatterns.behavioral.observer;

public class ObserverClient {

    public static void main(String[] args) {
        var publisher = new Publisher("publisher 0");
        var subscriber = new Subscriber();
        publisher.add(subscriber);
        publisher.add(new Subscriber());
        publisher.remove(subscriber);

        var publisher1 = new Publisher("publisher 1");
        publisher1.add(new Subscriber());
        publisher1.add(new Subscriber());

        publisher.notifySubscribers();
        publisher1.notifySubscribers();
    }

}
