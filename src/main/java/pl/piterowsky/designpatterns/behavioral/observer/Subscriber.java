package pl.piterowsky.designpatterns.behavioral.observer;

import java.util.Random;

public class Subscriber {

    private int id;

    public Subscriber() {
        id = new Random().nextInt();
    }

    public void update() {
        System.out.println("Updating subscriber " + id);
    }

    public int getId() {
        return id;
    }

}
