package pl.piterowsky.designpatterns.behavioral.iterator;

public interface BasicIterator<T> {

    boolean hasNext();
    T next();

}
