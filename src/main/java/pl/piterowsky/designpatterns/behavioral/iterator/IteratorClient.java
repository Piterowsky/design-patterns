package pl.piterowsky.designpatterns.behavioral.iterator;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Iterator it's a design pattern that lets you traverse elements of a collection without
 * exposing it's underlying representation
 */
public class IteratorClient {

    public static void main(String[] args) {
        var collection = IntStream.range(0, 13).boxed().collect(Collectors.toList());
        var evenIterator = new EvenIterator<>(collection);

        while(evenIterator.hasNext()) {
            System.out.println(evenIterator.next());
        }
    }

}
