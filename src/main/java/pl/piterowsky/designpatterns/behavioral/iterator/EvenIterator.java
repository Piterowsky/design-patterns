package pl.piterowsky.designpatterns.behavioral.iterator;

import java.util.List;

public class EvenIterator<T> implements BasicIterator<T> {

    private final List<T> collection;

    private int cursor;

    public EvenIterator(List<T> collection) {
        this.collection = collection;
        this.cursor = 0;
    }

    @Override
    public boolean hasNext() {
        return cursor + 2 <= collection.size();
    }

    @Override
    public T next() {
        if (hasNext()) {
            cursor += 2;
            return collection.get(cursor);
        }
        return null;
    }

}
