package pl.piterowsky.designpatterns.behavioral.iterator.failfast;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * When we are modifying structure of {@link ArrayList} while iterating with
 * fail fast iterator {@link ConcurrentModificationException} should be thrown
 */
public class FailFastIterator {

    public static void main(String[] args) {
        var arraylist = IntStream.range(0,100).boxed().collect(Collectors.toCollection(ArrayList::new));
        var iterator = arraylist.iterator();
        while(iterator.hasNext()) {
            var next = iterator.next();
            System.out.print(next + ",");
            if(next % 10 == 0) {
                arraylist.add(0);
            }
        }
    }

}
