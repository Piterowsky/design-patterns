package pl.piterowsky.designpatterns.behavioral.iterator.failsafe;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * Fail safe iterators works at copy of collections so modifying
 * collection during iteration doesn't make any trouble
 */
public class FailSafeIterator {

    public static void main(String[] args) {
        var arraylist = IntStream.range(0, 100).boxed().collect(Collectors.toCollection(CopyOnWriteArrayList::new));
        var iterator = arraylist.iterator();
        while(iterator.hasNext()) {
            var next = iterator.next();
            System.out.print(next + ",");
            if(next % 10 == 0) {
                arraylist.add(0);
            }
        }
    }

}
