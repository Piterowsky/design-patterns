package pl.piterowsky.designpatterns.behavioral.state.states;

import pl.piterowsky.designpatterns.behavioral.state.PhoneContext;

import java.util.function.IntConsumer;

public class UnlockedState extends AbstractState {

    private static final String DOING_NOTHING = "[UNLOCKED] Doing nothing...";

    public UnlockedState(PhoneContext phoneContext) {
        super(phoneContext);
    }

    @Override
    public void onSingleTap() {
        System.out.println(DOING_NOTHING);
    }

    @Override
    public void onDoubleTap() {
        System.out.println(DOING_NOTHING);
    }

    @Override
    public void onButton() {
        System.out.println("[UNLOCKED] Locking...");
        phoneContext.lock();
    }

    @Override
    public void onVolumeUp(int currentVolume, IntConsumer setCurrentVolume) {
        var volume = Math.min(currentVolume + 10, 100);
        setCurrentVolume.accept(volume);
        System.out.println("[UNLOCKED] Volume up... from " + currentVolume + " to " + volume);
    }

    @Override
    public void onVolumeDown(int currentVolume, IntConsumer setCurrentVolume) {
        var volume = Math.max(currentVolume - 10, 0);
        setCurrentVolume.accept(volume);
        System.out.println("[UNLOCKED] Volume down... from " + currentVolume + " to " + volume);
    }

}
