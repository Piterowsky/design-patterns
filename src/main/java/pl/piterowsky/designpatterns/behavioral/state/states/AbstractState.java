package pl.piterowsky.designpatterns.behavioral.state.states;

import pl.piterowsky.designpatterns.behavioral.state.PhoneContext;

public abstract class AbstractState implements State {

    protected final PhoneContext phoneContext;

    protected AbstractState(PhoneContext phoneContext) {
        this.phoneContext = phoneContext;
    }

}
