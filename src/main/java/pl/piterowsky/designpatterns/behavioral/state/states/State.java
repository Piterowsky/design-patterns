package pl.piterowsky.designpatterns.behavioral.state.states;

import java.util.function.IntConsumer;

public interface State {

    void onSingleTap();
    void onDoubleTap();
    void onButton();
    void onVolumeUp(int currentVolume, IntConsumer setCurrentVolume);
    void onVolumeDown(int currentVolume, IntConsumer setCurrentVolume);

}
