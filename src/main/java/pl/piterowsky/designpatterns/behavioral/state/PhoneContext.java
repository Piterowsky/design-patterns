package pl.piterowsky.designpatterns.behavioral.state;

import pl.piterowsky.designpatterns.behavioral.state.states.LockedState;
import pl.piterowsky.designpatterns.behavioral.state.states.State;
import pl.piterowsky.designpatterns.behavioral.state.states.UnlockedState;

public class PhoneContext {

    private State currentState;
    private int currentVolume;

    public PhoneContext() {
        this.currentState = new LockedState(this);
    }

    public void unlock() {
        if (currentState instanceof LockedState) {
            this.currentState = new UnlockedState(this);
        }
    }

    public void lock() {
        if (currentState instanceof UnlockedState) {
            this.currentState = new LockedState(this);
        }
    }

    public void singleTap() {
        currentState.onSingleTap();

    }

    public void doubleTap() {
        currentState.onDoubleTap();
    }

    public void clickButton() {
        currentState.onButton();
    }

    public void volumeUp() {
        currentState.onVolumeUp(currentVolume, this::setCurrentVolume);
    }

    public void volumeDown() {
        currentState.onVolumeDown(currentVolume, this::setCurrentVolume);
    }

    private void setCurrentVolume(int currentVolume) {
        this.currentVolume = currentVolume;
    }

}
