package pl.piterowsky.designpatterns.behavioral.state;

public class StateClient {

    public static void main(String[] args) {
        var phone = new PhoneContext();
        phone.lock();
        phone.volumeDown();
        phone.volumeUp();
        phone.unlock();
        phone.volumeUp();
        phone.volumeDown();
        phone.doubleTap();
        phone.singleTap();
        phone.lock();
        phone.doubleTap();
        phone.singleTap();
    }

}
